//
//  main.m
//  Cube
//
//  Created by Dalia on 1/27/13.
//  Copyright (c) 2013 Dalia. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CubeAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CubeAppDelegate class]));
    }
}
